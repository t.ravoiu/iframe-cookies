const express = require('express')
const cookieParser = require('cookie-parser');

const app = express()
app.use(cookieParser('secret'))

const port = 3000

app.get('/', (req, res) => {
    res.send(`<form action="/login" method="post"><input type="submit" value="Submit"></form> `)
})

app.post('/login', (req, res) => {
    let options = {
        maxAge: 1000 * 60 * 15,
        httpOnly: true,
        signed: true,
        sameSite: 'none',
        secure: true
    }

    // Set cookie
    res.cookie('loginCookie', 'cookieValue', options) // options is optional

    res.send('Logged in')
})

app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
})