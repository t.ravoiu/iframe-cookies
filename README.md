# How to use this?
1. Start the server
   
   `$ cd server`
   `$ npm i`
   `$ node test.js`

2. Run the webpage with the iframe integrated. Ideally in VSCode with the [Live Server](https://marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer) extension